load('Event_name.mat');
concepts = containers.Map;
for i = 1:length(events)
    fid = fopen(['./Total_Records/' strrep(events{i},' ','_') '/concept_recorder.txt']);
    while ~feof(fid)
        line = fgetl(fid);
        line = regexp(line,'\t','split');
        line = line{1};
        line = line(1:end-1);
        if strcmp(line,'other') == 1
            continue;
        end
        concepts(line) = 1;
    end
    fclose(fid);
end
concepts = keys(concepts);