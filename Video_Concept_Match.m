clear;
load('./concepts.mat');
load('./video_ids.mat');
load('./Event_name.mat');
num_concepts = length(concepts);
num_events = length(events);
num_videos = length(video_ids);
con = containers.Map;
evn = containers.Map;
vid = containers.Map;
idset = containers.Map;
for i = 1:num_concepts
    con(concepts{i}) = i;
end
for i = 1:num_events
    evn(events{i}) = i;
end
for i = 1:length(video_ids)
    vid(video_ids{i}) = i;
end

%%
Matching_mat = zeros(num_videos,num_concepts+num_events);
overlap = 0;
for i = 1:length(events)
    fprintf('Processing at %d.\n',i);
    fid = fopen(['./Total_Records/' strrep(events{i},' ','_') '/concept_recorder.txt']);
    while ~feof(fid)
        line = fgetl(fid);
        words = regexp(line,'\t','split');
        cpt = strtrim(words{1});
        cpt = cpt(1:end-1);
        for j = 2:length(words)
            id = strtrim(words{j});
            if ~isKey(vid,id)
                continue;
            end
            if isKey(idset,id)
                overlap = overlap + 1;
                fprintf('!!Attention!!\n');
                continue;
            else
                idset(id) = 1;
            end
            if strcmp(cpt,'other') ~=1
                Matching_mat(vid(id),con(cpt)) = 1;
            end
            
            Matching_mat(vid(id),num_concepts + evn(events{i})) = 1;
        end
        
    end
    fclose(fid);
end
save(['./Matching_mat.mat'],'Matching_mat');
