writer = fopen('event_concept.txt','w+');
load('Event_Name.mat');
for i = 1:500
    fid = fopen(['./Event_Record\' strrep(events{i}, ' ' ,'_') '.txt']);
    fprintf(writer,events{i});
    while ~feof(fid)
        line = fgetl(fid);
        line = regexp(line,'\t','split');
        line = line{1};
        line = line(1:end-1);
        if strcmp(line,'other') ~=1 && strcmp(line,'world') ~=1 && strcmp(line,'tv') ~=1 && strcmp(line,'type')~=1
        fprintf(writer,['\t' line]);
        end
    end
    fprintf(writer,'\n');
    fclose(fid);
end
fclose(writer);

fid = fopen('event_concept.txt');
concepts = containers.Map;
while ~feof(fid)
line = fgetl(fid);
line = regexp(line,'\t','split');
for i = 2:length(line)
concepts(line{i}) = 1;
end
end
fclose(fid);
concepts = keys(concepts);
concepts = concepts';
save(['concepts.mat'],'concepts');