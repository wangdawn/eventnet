load('Event_Name.mat');
load('Matching_mat.mat');
load('concepts.mat');
concepts_num = length(concepts);

evts = containers.Map;
cpts = containers.Map;
cpt_evt_matching = zeros(10000,2);
for i = 1:length(events)
    evts(events{i}) = i;
end
for i = 1:length(concepts)
    cpts(concepts{i}) = i;
end
count = 0;
for i = 1:length(events)
    fid = fopen(['./Total_Record/' strrep(events{i},' ','_') '/concept.txt']);
    while ~feof(fid)
        line = fgetl(fid);
        line = regexp(line,'\t','split');
        line = line{1};
        line = line(1:end-1);
        if strcmp(line,'other') == 1
            continue;
        end
        evt_idx = i;
        cpt_idx = cpts(line);
        if length(Matching_mat(:,cpt_idx) & Matching_mat(:,concepts_num + evt_idx)) >3
            count = count + 1;
            cpt_evt_matching(count,:) = [evt_idx cpt_idx];
        end
    end
    fclose(fid);
end

cpt_evt_matching = cpt_evt_matching(1:count,:);