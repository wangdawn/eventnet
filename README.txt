This folder is to get the relationship of event-concept and event-video.

Step1: Arrange files as in Total_Records. Related files can be found after download videos using search_and_download.py

Step2: Collect all the video ids of your downloaded videos as video_ids.mat and your event names as Event_name.mat.

Step3: Run Get_concepts.m and get all the unique concepts.

Step4: Run Video_Concept_Match.m.

Step5(optional): Run Write_Event_Concept.m to get a .txt file, which is the same as ./BackUp/EventNet_Final_Ontology/event_concept.txt.

